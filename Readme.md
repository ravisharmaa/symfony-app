# Challenge By CobbleWeb

## Description
This was challenge task from Cobble web.

## Dependencies
The following dependencies are required to successfully run this project:
- Docker
- Make (Optional)


## Setup Instructions
Assuming that docker & make is already available in the machine
`docker compose up -d`
The compose file adds a mail server as well and also enabled debug mode for php. As the base docker-compose file comes up with Traefik proxy installed which should serve the applcation in `http://app.localhost` domain after running the up commands.
## Libraries Used
1. Symfony and Symfony Components
2. Lexik JWT Auth

## Tools Used
- Rabbit MQ For Queue
- Maildev for testing mails locally.


## Solution Formulation
Steps I thought of when I was trying to work on this task.
1. Creating user and photo entities.
2. Adding proper docker-compose and docker file which address the easy browsing procedure.
3. Adjusting the schema to properly normalize the tables.
4. Handling the routes properly using proper authentication.

## Decisions Tradeoffs & Constraints
While working on the solution I made some conscious decisions which are:
1. Removing the photos column
    - I believe the photos column was not necessary in the user table. It would violate normalization principles. Hence I altered the schema to best scale the system.
2. Queue Driver and Messenger
    - I consciously chose rabbit mq as the queue driver for the following reasons:
    - Saving photos can be an async task because we usually do not want for the user to wait for uploading photos to S3. In the case of the local directory I made it sync.
    - Sending emails via the cron can also be an async task because, we do not want to fail if we are unable to send email to one user, we can configure the retry mechanism.
3. Usage of Strategy for Cloud and Local Photo Upload
    - I thought of using the strategy pattern to implement the photo upload between local and s3. As a test I wanted to demonstrate my knowledge on patterns and I believe I accomplished that in this case.
   
## Improvements
1. More code refactor
2. The file upload to s3 does not seem to work might be the keys are wrongly pasted in my machine I would need some more time to debug. I believe I showed the procedure and fixing the bug related to that should be a fraction of time
