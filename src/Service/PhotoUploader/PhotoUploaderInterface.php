<?php

namespace App\Service\PhotoUploader;

use App\Enums\UploadDirectory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface PhotoUploaderInterface
{
    public function upload(UploadedFile $file, UploadDirectory $uploadDirectory): ?string;
}