<?php

namespace App\Service\PhotoUploader;

use App\Enums\UploadDirectory;
use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CloudFileSystemUploader implements PhotoUploaderInterface
{
    public function __construct(
        private readonly S3Client $client,
        private readonly string $bucket
    ) {
    }

    public function upload(UploadedFile $file, UploadDirectory $uploadDirectory): ?string
    {
        $result = $this->client->putObject(
            [
                'Bucket' => $this->bucket,
                'Key' => sprintf("%s/%s", $uploadDirectory->getPath(), $file->getFilename()),
                'Body' => $file->getContent(),
            ]
        );

        return $result->get("ObjectURL") ?? null;
    }
}