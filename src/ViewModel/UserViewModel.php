<?php

namespace App\ViewModel;

use App\Entity\User;

class UserViewModel implements \JsonSerializable
{
    public function __construct(
        private readonly User $user
    ) {
    }

    public function jsonSerialize(): array
    {
        $data = [];
        $data['user'] = [
            'email' => $this->user->getEmail(),
            'avatar' => $this->user->getAvatar(),
            'first_name' => $this->user->getFirstName(),
            'last_name' => $this->user->getLastName(),
            'photos' => $this->user->getPhotos()->count()
        ];

        return $data;
    }
}