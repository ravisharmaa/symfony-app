<?php

namespace App\Service;

use App\Entity\Photo;
use App\Entity\User;
use App\Enums\UploadDirectory;
use App\Exception\ValidationException;
use App\Message\UploadPhotosToS3;
use App\Service\PhotoUploader\PhotoUploaderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService
{
    private CONST DEFAULT_AVATAR = 'default-avatar.png';

    public function __construct(
        private readonly EntityManagerInterface      $entityManager,
        private readonly ValidatorInterface          $validator,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly PhotoUploaderInterface      $uploader,
        private readonly MessageBusInterface          $messageBus
    ) {
    }

    public function register(Request $request): User
    {
        $user = new User();
        $user->setFirstName($request->request->get('firstName'));
        $user->setLastName($request->request->get('lastName'));
        $user->setEmail($request->request->get('email'));
        $password = $request->request->get('password');

        $errors = $this->validator->validatePropertyValue($user, 'password', $password);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        if ($request->files->has('avatar')) {
            $avatar = $this->uploader->upload($request->files->get('avatar'), UploadDirectory::AVATAR);
            $user->setAvatar($avatar);
        } else {
            $user->setAvatar(self::DEFAULT_AVATAR);
        }

        $hashedPassword = $this->passwordHasher->hashPassword($user, $password);
        $user->setPassword($hashedPassword);

        $violations = $this->validator->validate($user);
        if (count($request->files->get('photos')) <= 3) {
            $violations->add(new ConstraintViolation(
                '4 photos are needed.',
                '',
                [],
                null,
                'photos',
                null
            ));
        }

        if (count($violations) > 0) {
            throw new ValidationException($violations);
        }

        $photosArray = [];

        foreach ($request->files->get('photos') as $photoItem) {
            $fileName = $this->uploader->upload($photoItem, UploadDirectory::PHOTOS);
            $photo = new Photo();
            $photo->setName($fileName);
            $photo->setUser($user);
            $photosArray[] = [
                'photo' => $fileName,
                'photoObject' => $photo
            ];
            $this->entityManager->persist($photo);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->messageBus->dispatch(new UploadPhotosToS3($photosArray));

        return $user;

    }
}