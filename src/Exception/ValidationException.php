<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationList;

class ValidationException extends \RuntimeException
{
    public function __construct(
        private readonly ConstraintViolationList $violationList
    ) {
        parent::__construct("Validation Failed", 400, null);
    }

    public function formatViolationErrors(): array
    {
        $result = [];
        foreach ($this->violationList as $violation) {
            $property = trim($violation->getPropertyPath(), '[]');
            $result[$property] = $violation->getMessage();
        }

        return $result;
    }
}