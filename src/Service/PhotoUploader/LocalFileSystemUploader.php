<?php

namespace App\Service\PhotoUploader;

use App\Enums\UploadDirectory;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LocalFileSystemUploader implements PhotoUploaderInterface
{
    public function __construct(
        private readonly LoggerInterface $logger
    ) {
    }

    public function upload(UploadedFile $file, UploadDirectory $uploadDirectory): ?string
    {
        $fileName = Uuid::uuid4()->toString() . '.' . $file->getClientOriginalExtension();
        try {
            $file->move($uploadDirectory->getPath(), $fileName);
        } catch (FileException $exception) {
            $this->logger->info(
                'Cannot upload file', [
                'message' => $exception->getMessage()
                ]
            );

            return null;
        }

        return $fileName;
    }
}