<?php

namespace App\Message;

use App\Entity\Photo;
use App\Entity\User;

final class UploadPhotosToS3
{
    public function __construct(
        private readonly array $photos,
    ) {
    }

    public function getPhotos(): array
    {
        return $this->photos;
    }
}
