<?php

namespace App\Command;

use App\Message\SendNewsLetterMessage;
use App\Repository\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'app:send:news-letters',
    description: 'Sends news letters to active users',
)]
class SendNewsLetterToActiveUsersCommand extends Command
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly MessageBusInterface $bus
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $activeUsers = $this->userRepository->findBy(['active' => true]);
        foreach ($activeUsers as $user) {
            $this->bus->dispatch(new SendNewsLetterMessage($user->getEmail()));
        }
        return Command::SUCCESS;
    }
}
