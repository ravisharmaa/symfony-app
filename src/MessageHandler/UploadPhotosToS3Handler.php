<?php

namespace App\MessageHandler;

use App\Entity\Photo;
use App\Enums\UploadDirectory;
use App\Message\UploadPhotosToS3;
use App\Service\PhotoUploader\PhotoUploaderInterface;
use Aws\S3\Exception\S3Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class UploadPhotosToS3Handler
{
    public function __construct(
        private readonly PhotoUploaderInterface $uploader,
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $entityManager,
        private readonly string $localFilePath
    ) {
    }

    public function __invoke(UploadPhotosToS3 $message): void
    {
        /**
         * @var $photo UploadedFile
         */
        foreach ($message->getPhotos() as $photo) {
            try {
                /**
                 * @var Photo $photoObject
                 */
                $photoObject = $photo['photoObject'];
                $photoUrl = $this->uploader->upload($this->transformToUploadedFile($photoObject), UploadDirectory::S3BUCKET);
                $photoObject->setUrl($photoUrl);
                $this->entityManager->persist($photoObject);
            } catch (S3Exception $exception) {
                $this->logger->info(
                    'Could not upload file to s3', [
                    'exception' => $exception
                    ]
                );
                continue;
            }
        }
        $this->entityManager->flush();
    }

    private function transformToUploadedFile(Photo $photo): UploadedFile
    {
        $mime = mime_content_type(__DIR__ . '/../../public/'. $this->localFilePath . '/' .$photo->getName());
        $size = filesize(__DIR__ . '/../../public/'. $this->localFilePath . '/' . $photo->getName());

        return new UploadedFile(
            __DIR__ .'/../../public/user/photos/'. $photo->getName(),
            $photo->getName(),
            $mime,
            $size,
            true
        );
    }
}
