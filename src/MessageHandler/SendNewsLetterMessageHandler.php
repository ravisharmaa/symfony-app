<?php

namespace App\MessageHandler;

use App\Message\SendNewsLetterMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

#[AsMessageHandler]
final class SendNewsLetterMessageHandler
{
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly LoggerInterface $logger
    ) {
    }

    public function __invoke(SendNewsLetterMessage $message): void
    {
        try {
            $this->mailer->send($message->toEmail());
        } catch (TransportExceptionInterface $exception) {
            $this->logger->info("Failed sending message");
        }
    }
}
