<?php

namespace App\Message;

use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

final class SendNewsLetterMessage
{
    public function __construct(
        private readonly string $to
    ) {
    }

    public function toEmail(): Email
    {
        return (new Email())
            ->to($this->to)
            ->from(new Address('test@cobbleweb.com', 'Cobble Web'))
            ->subject('Your best newsletter')
            ->text(
                <<<MAIL
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, whe
MAIL
            );
    }
}
