<?php

namespace App\Controller\Api;

use App\Exception\ValidationException;
use App\Service\UserService;
use App\ViewModel\UserViewModel;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthController extends AbstractController
{
    public function register(
        UserService $userService,
        Request     $request
    ): JsonResponse {
        try {
            $user = $userService->register($request);
        } catch (ValidationException $exception) {
            return $this->json(
                [
                'code' => 400,
                'errors' => $exception->formatViolationErrors(),
                ], 400
            );
        }

        return $this->json(
            [
            'user' => $user->getEmail()
            ]
        );
    }

    public function login(
        UserInterface $user, JWTTokenManagerInterface $tokenManager
    ): JsonResponse {
        return new JsonResponse(
            [
            'token' => $tokenManager->create($user)
            ]
        );
    }

    public function profile(): JsonResponse
    {
        $user = $this->getUser();
        return new JsonResponse((new UserViewModel($user))->jsonSerialize());
    }
}
