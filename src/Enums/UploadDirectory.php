<?php

namespace App\Enums;

enum UploadDirectory
{
    case PHOTOS;
    case AVATAR;
    case S3BUCKET;

    public function getPath(): string
    {
        return match ($this) {
            self::PHOTOS => 'user/photos/',
            self::AVATAR => 'user/avatars',
            self::S3BUCKET => 'photos'
        };
    }
}