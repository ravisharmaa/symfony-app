FROM php:8.2-fpm
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/install-php-extensions


RUN apt-get update && apt-get -y --no-install-recommends install cron && \
 apt-get -y install supervisor && \
  mkdir -p /var/log/supervisor && \
  mkdir -p /etc/supervisor/conf.d \
  && rm -rf /var/lib/apt/lists/*

# Install required PHP extensions and all their prerequisites available via apt.
RUN chmod uga+x /usr/bin/install-php-extensions \
    && sync \
    && install-php-extensions pdo_mysql mysqli amqp

COPY cronjobs /etc/cron.d/cronjobs
# Apply permissions
RUN chmod 0644 /etc/cron.d/cronjobs
# Run crontab
RUN crontab /etc/cron.d/cronjobs
# Create log file where the result of running the cronjobs will be saved
RUN touch /var/log/cron.log
# Copy supervisord
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

CMD ["/usr/bin/supervisord"]